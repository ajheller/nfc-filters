# Ambisonic Near-field Compensation Filters #

### What is this repository for? ###

* This is a Python module of SymPy code to create, manipulate, and verify Bessel Polynomials and reverse Bessel Polynomials. 
* Use them to create Ambisonic near-field correction (NFC) filters.
* Version 1.0

### References
1.  H. L. Krall and O. Frink, "A New Class of Orthogonal Polynomials: The
    Bessel Polynomials," Transactions of the American Mathematical Society,
    vol. 65, no. 1, pp. 100�115, Jan. 1949.

2.  J. Daniel, "Spatial Sound Encoding Including Near Field Effect:
    Introducing Distance Coding Filters and a Viable, New Ambisonic Format,"
    Preprints 23rd AES International Conference, Copenhagen, 2003.

3.  Wikipedia contributors. "Bessel polynomials." Wikipedia, The Free
    Encyclopedia. Wikipedia, The Free Encyclopedia, 28 Mar. 2017. Web.
    17 Dec. 2017.
   
4.  Wikipedia contributors, "Bessel filter." Wikipedia, The Free
    Encyclopedia. Wikipedia, The Free Encyclopedia, 20 Sep. 2017. Web. 17
    Jan. 2018.

5.  Julius O. Smith III, "Digital State-Variable Filters," 
    https://ccrma.stanford.edu/~jos/svf/


### Author ###

* Author: Aaron Heller <heller@ai.sri.com>

