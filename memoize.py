#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 18 08:39:11 2017

@author: heller
"""

from __future__ import print_function


_debug = True


def memoize(f):
    """ Memoization decorator for functions taking one or more arguments. """
    class memodict(dict):
        def __init__(self, f):
            self.f = f

        def __call__(self, *args):
            return self[args]

        def __missing__(self, key):
            ret = self[key] = self.f(*key)
            return ret

    return memodict(f)


@memoize
def fib(n):
    if n < 0:
        raise ValueError("n should be non-negative")

    if _debug:
        print(n)

    if n == 0:
        return 1
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)


@memoize
def fact(n):
    if n < 0:
        raise ValueError("n should be non-negative")

    if _debug:
        print(n)

    if n == 0:
        return 1
    else:
        return n * fact(n-1)


# @memoize
def ackermann(m, n, depth=0):
    print(" "*depth, "call:", m, n)
    if m == 0:
        print(" "*depth, "m==0", m, n)
        a = n + 1
    elif m > 0 and n == 0:
        print(" "*depth, "m>0 and n==0", m, n)
        a = ackermann(m-1, 1, depth+1)
    elif m > 0 and n > 0:
        print(" "*depth, "m>0 and n>0", m, n)
        a = ackermann(m-1, ackermann(m, n-1, depth+1))
    else:
        raise ValueError("m and n should be non-negative")

    print(" "*depth, "ret:", m, n, "-->", a)
    return a

