#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23 16:27:05 2017

@author: heller
"""
from __future__ import division, print_function

from memoize import memoize
from sympy import factorial, root
from sympy import re, im
from sympy import N, pi
from sympy import symbols, Poly
from sympy import expand, simplify, diff
from sympy.polys.polytools import poly
from sympy.polys.polytools import nroots

from matplotlib import pyplot


x = symbols('x')


"""References:

[1] H. L. Krall and O. Frink, "A New Class of Orthogonal Polynomials: The
    Bessel Polynomials," Transactions of the American Mathematical Society,
    vol. 65, no. 1, pp. 100–115, Jan. 1949.

[2] J. Daniel, "Spatial Sound Encoding Including Near Field Effect:
    Introducing Distance Coding Filters and a Viable, New Ambisonic Format,"
    Preprints 23rd AES International Conference, Copenhagen, 2003.

[3] Wikipedia contributors. "Bessel polynomials." Wikipedia, The Free
    Encyclopedia. Wikipedia, The Free Encyclopedia, 28 Mar. 2017. Web.
    17 Dec. 2017.

[4] Wikipedia contributors. "Bessel filter." Wikipedia, The Free
Encyclopedia. Wikipedia, The Free Encyclopedia, 20 Sep. 2017. Web. 17
Jan. 2018.

"""

_debug = False  # True


# IEEE quadprecsion has 33 to 36 decimal digits of precision
#  https://en.wikipedia.org/wiki/Quadruple-precision_floating-point_format
_ndigits = 38


@memoize
def y(n):
    if n == 0:
        p = 1                                  # y_0
    elif n == 1:
        p = x + 1                              # y_1
    else:
        p = expand((2*n-1)*x*y(n-1) + y(n-2))  # y_n

    if _debug:
        print(n, p)
    return p


@memoize
def th(n):
    if n == 0:
        p = 1
    elif n == 1:
        p = x + 1
    else:
        p = expand((2*n-1)*th(n-1) + x**2*th(n-2))

    if _debug:
        print(n, p)
    return p


def bessel_poly_pure(degree, reverse=True, debug=False):
    if degree < 0:
        raise ValueError("degree should be non-negative")
    if reverse:
        p = th(degree)
    else:
        p = y(degree)

    return simplify(p)

# b_diff_eq = x**2 * diff(y, x, x) + (2*x + 2) * diff(y, x) - n*(n+1) * y

# The following check various properties of Bessel polynomials


def bessel_diff_eq(n):
    "Bessel polynomials satsify this differential equation"
    y = bessel_poly_pure(n, reverse=False)
    diff_eq = x**2 * diff(y, x, x) + (2*x + 2) * diff(y, x) - n*(n+1) * y
    return simplify(diff_eq) == 0


def reverse_bessel_diff_eq(n):
    th = bessel_poly_pure(n, reverse=True)
    diff_eq = x*diff(th, x, x) - 2*(x+n)*diff(th, x) + 2*n*th
    return simplify(diff_eq) == 0


def test_bessel_polys_inverse(n):
    sub = {x: 1/x}
    y_n = y(n)
    th_n = th(n)

    iy_n = simplify(x**n * y(n).subs(sub))
    ith_n = simplify(x**n * th(n).subs(sub))

    return (y_n == ith_n, th_n == iy_n)


def bessel_poly_r(degree, reverse=True):
    y = []
    for i in range(degree+1):
        if i == 0:
            y.append(1)
        elif i == 1:
            y.append(1+x)
        else:
            if reverse:
                # the reverse recurrance relation
                y.append(expand((2*i-1)*y[i-1] + x**2*y[i-2]))
            else:
                y.append(expand((2*i-1)*x*y[i-1] + y[i-2]))

    return y


def reverse_poly(p):
    return Poly(list(p.as_coefficients_dict().itervalues()), x)


def bessel_coeff(degree, term):
    "return the coefficient of the 'term'-th term of the Bessel Polynomial"

    if term < 0 or term > degree:
        raise ValueError("term should be between 0 and degree")

    if term == 0:
        a = 1
    else:
        a = factorial(degree+term) / \
            (factorial(degree-term) * factorial(term) * 2**term)
    return a


def bessel_poly(degree, reverse=False, x_var=x):
    return poly(sum((x_var**term *
                     bessel_coeff(degree, degree-term if reverse else term)
                     for term in range(0, degree+1))),
                x_var)


def bessel_poly_roots(degree, reverse=False, n=_ndigits):
    p = bessel_poly(degree, reverse=reverse)
    # 40 digits to support FAUST quad precision
    r = nroots(p, n=n, maxsteps=1000)
    return r


def poly_factors(p, n=_ndigits):
    roots = nroots(p, n=n, maxsteps=1000)
    sroots = sorted(roots, key=im, reverse=True)

    # get the coefficent of the constant degree term
    #  current implemenation assumes it is 1
    a_d = p.coeff_monomial(1)  # FIXME: should check that it is 1

    factors = []
    for r in sroots:
        a = re(r)
        b = im(r)
        if b > 0:
            # s = expand((x-r)*(x-r.conjugate()))
            a0 = a**2 + b**2
            s = poly((x**2 - 2*a*x + a0) / a0)
            factors.append(s)
        elif b == 0:
            a0 = -a
            s = poly((x + a0) / a0)
            factors.append(s)
        else:  # b < 0
            pass  # already handled

    return factors


def plot_roots(roots):
    lim = float(max([abs(r) for r in roots]))

    pyplot.plot(
            [re(r) for r in roots],
            [im(r) for r in roots],
            'bo')
    pyplot.axis([-lim, lim, -lim, lim])
    pyplot.show()


