#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 16:29:25 2018

@author: heller
"""
from __future__ import division, print_function

from sympy import re, im
from sympy import N, pi
from sympy import expand
from sympy.polys.polytools import nroots

from bessel_poly import bessel_poly, poly_factors, x


# Support FAUST quad precision
# IEEE quadprecsion has 33 to 36 decimal digits of precision
#  https://en.wikipedia.org/wiki/Quadruple-precision_floating-point_format
_ndigits = 38


def write_nfc_filter_sections(degree, n=_ndigits):
    ""
    bp = bessel_poly(degree)
    factors = poly_factors(bp)

    # print out the FAUST for the filter
    print("nfc({degree}, distance) = _ :".format(**locals()))
    for f in factors:
        if f.degree() == 2:
            a2 = f.coeff_monomial(x**2)
            a1 = f.coeff_monomial(x)
            print("   nfc2x(distance, {a1}, {a2}) :".format(**locals()))

        elif f.degree() == 1:
            a1 = f.coeff_monomial(x)
            print("   nfc1x(distance, {a1}) :".format(**locals()))

        else:
            pass  # FIXME: should raise an error here
    print("   _ ;")

    return factors


faust_nfc = """
declare name		"nfc_filters";
declare version 	"1.3";
declare author 		"AmbisonicDecoderToolkit";
declare license 	"BSD 3-Clause License";
declare copyright	"(c) Aaron J. Heller 2013, 2017";

// References:
//
// [1] H. L. Krall and O. Frink, "A New Class of Orthogonal Polynomials: The
//     Bessel Polynomials," Transactions of the American Mathematical Society,
//     vol. 65, no. 1, pp. 100–115, Jan. 1949.
//
// [2] J. Daniel, "Spatial Sound Encoding Including Near Field Effect:
//     Introducing Distance Coding Filters and a Viable, New Ambisonic Format,"
//     Preprints 23rd AES International Conference, Copenhagen, 2003.
//
// [3] Wikipedia contributors, "Bessel polynomials." Wikipedia, The Free
//     Encyclopedia. Wikipedia, The Free Encyclopedia, 28 Mar. 2017. Web.
//     17 Dec. 2017.
//
// [4] Wikipedia contributors, "Bessel filter." Wikipedia, The Free
//     Encyclopedia. Wikipedia, The Free Encyclopedia, 20 Sep. 2017. Web. 17
//     Jan. 2018.
//
// [5] Julius O. Smith III, "Digital State-Variable Filters,"
//     https://ccrma.stanford.edu/~jos/svf/


m = environment {
  // from the old math.lib
  take (1, (xs, xxs)) = xs;
  take (1, xs) = xs;
  take (nn, (xs, xxs)) = take (nn-1, xxs);

  bus(2) = _,_; // avoids a lot of "bus(1)" labels in block diagrams
  bus(n) = par(i, n, _);

  SR = min(192000, max(1, fconstant(int fSamplingFreq, <math.h>)));
  //PI = 3.1415926535897932385;
  // quad precision value
  //PI = 3.1415926535897932384626433832795028842;
  PI = %s;
};

// m.SR from math.lib is an integer, we need a float
SR = float(m.SR);

temp_celcius = 20;
c = 331.3 * sqrt(1.0 + (temp_celcius/273.15)); // speed of sound m/s

// We implement the NFC fitlers as state variable filters for a few reasons:
// . in NFC filters Fc << Fs, which leads to round off errors in biquads.
//
// . NFC filters are Bessel filters and [4] notes that "As the
//   important // characteristic of a Bessel // filter is its
//   maximally-flat group delay, and not the amplitude // response, it is
//   inappropriate to use the bilinear transform to convert // the analog
//   Bessel filter into a digital form (since this preserves the //
//   amplitude response but not the group delay)."
//
//  State Variable Filters
//  TODO: note that svf1(g,d1) does the same computation as svf2(g,d1,0).
//        Does Faust optimize the latter into the former (i.e. get rid of the
//        2nd-order loop) or should I keep the former?
svf1a(g,d1)   = _ : *(g) :    ((_,_):> _ <: +~_, _ ) ~ *(d1)                   : !,_ ;
svf1(g,d1)    = _ : *(g) :          (+   <: +~_, _ ) ~ *(d1)                   : !,_ ;
svf2(g,d1,d2) = _ : *(g) : (((_,_,_):> _ <: +~_, _ ) ~ *(d1) : +~_, _) ~ *(d2) : !,_ ;

// polynomial is a1*x + a0, we assume a0 = 1
nfc1x(radius,a1) = svf1(g,d1) // r in meters
  with {
    // BLaH3: For point sources, the frequency at which the reactive and real
    // components [of the soundfield] are equal, f = c / (2 pi d)
    // where c is the speed of sound and d is the distance from the
    // loudspeaker.

    omega = c/float(radius);
    sT = omega/(2.0*SR);

    b1 = a1 * sT;
    g = 1.0 / (1.0 + b1);

    d1 = 0.0 - (2.0*b1) * g;
  };

// polynomial is a2*x^2 + a1*x + a0, we assume a0 = 1
nfc2x(radius,a1,a2) = svf2(g,d1,d2)
  with {
    omega = c/float(radius);
    sT = omega/(2.0*SR);

    b1 = a1 * sT;
    b2 = a2 * sT**2;
    g = 1.0 / (1.0 + b1 + b2);

    d1 = 0.0 - (2.0*b1 + 4.0*b2) * g;
    d2 = 0.0 - (4.0*b2) * g;
  };


""" % str(pi.evalf(_ndigits))  # .format(**locals())


def write_nfc_filters(max_degree, n=_ndigits):
    print(faust_nfc)
    for i in range(max_degree+1):
        write_nfc_filter_sections(i, n=n)
        print()
